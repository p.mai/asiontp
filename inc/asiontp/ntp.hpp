#ifndef NTP_HPP
#define NTP_HPP

// data types
#include <cstdint>
// memcpy
#include <cstring>

// TODO: maybe there's an os independent way to do this?
// hton
#include <netinet/in.h>

#include <string>
#include <sstream>
#include <cmath>

/// all of this can be taken from rfc 5905 https://datatracker.ietf.org/doc/html/rfc5905
namespace ntp {

/// basically a raw package as received, not considering endianness
struct network_package {
    uint8_t li_vn_mode;
    uint8_t stratum;
    uint8_t poll;
    uint8_t precision;

    uint16_t root_delay_seconds;
    uint16_t root_delay_fraction;

    uint16_t root_dispersion_seconds;
    uint16_t root_dispersion_fraction;

    uint32_t reference_id;

    uint32_t reference_timestamp_seconds;
    uint32_t reference_timestamp_fraction;

    uint32_t origin_timestamp_seconds;
    uint32_t origin_timestamp_fraction;

    uint32_t receive_timestamp_seconds;
    uint32_t receive_timestamp_fraction;

    uint32_t transmit_timestamp_seconds;
    uint32_t transmit_timestamp_fraction;

    static inline network_package from_buffer(const unsigned char* buffer, std::size_t len) {
        network_package package;

        if(len > 48) {
            // discard anything beyond
            std::memcpy(&package, buffer, 48);
        }
        else {
            std::memcpy(&package, buffer, len);
        }
        return package;
    }
};

/// "In the date and timestamp formats, the prime epoch, or base date of
/// era 0, is 0 h 1 January 1900 UTC, when all bits are zero." quoted from rfc.
/// This can be used to convert to posix utc begin of epoch:
static constexpr unsigned long epoch_seconds_1900_to_1970 = 2208988800;

struct ntp_short {
    uint16_t seconds;
    uint16_t fraction;
};

struct ntp_timestamp {
    // note: seconds since 1900!
    // use epoch_seconds_1900_to_1970 to convert to utc
    uint32_t seconds;
    uint32_t fraction;
};

struct ntp_date {
    uint32_t era_number;
    uint32_t era_offset;
    uint64_t fraction;
};

enum leap_indicator {
    NO_WARNING,
    LAST_MINUTE_OF_DAY_61_SECONDS,
    LAST_MINUTE_OF_DAY_59_SECONDS,
    UNKNOWN_OR_UNSYNCHRONIZED
};

enum op_mode {
    RESERVED,
    SYMMETRIC_ACTIVE,
    SYMMETRIC_PASSIVE,
    CLIENT,
    SERVER,
    BROADCAST,
    NTP_CONTROL_MESSAGE,
    RESERVED_FOR_PRIVATE_USE
};

struct ntp_header {
    leap_indicator li;
    op_mode mode;
    uint8_t version;

    // 0: unspecified or invalid
    // 1: primary server (e.g., equipped with a GPS receiver)
    // 2-15: secondary server (via NTP)
    // 16: unsynchronized
    // 17-255: reserved
    uint8_t stratum;

    /* Poll: 8-bit signed integer representing the maximum interval between
       successive messages, in log2 seconds.  Suggested default limits for
       minimum and maximum poll intervals are 6 and 10, respectively. */
    int8_t poll;

    /* Precision: 8-bit signed integer representing the precision of the
       system clock, in log2 seconds.  For instance, a value of -18
       corresponds to a precision of about one microsecond.  The precision
       can be determined when the service first starts up as the minimum
       time of several iterations to read the system clock. */
    int8_t precision;

    ntp_short root_delay;
    ntp_short root_dispersion;

    // stratum == 0 means this is a KoD signal,
    // stratum == 1 means ascii identifier of reference clock,
    // otherwise, reference id depends on protocol:
    // ipv4: ip addresse of server
    // ipv6: first four octets of the MD5 hash of the IPv6 address
    char reference_id[4];

    // Reference Timestamp: Time when the system clock was last set or corrected, in NTP timestamp format.
    ntp_timestamp reference_timestamp;
    // Origin Timestamp (org): Time at the client when the request departed for the server, in NTP timestamp format.
    ntp_timestamp origin_timestamp;
    // Receive Timestamp (rec): Time at the server when the request arrived from the client, in NTP timestamp format.
    ntp_timestamp receive_timestamp;
    // Transmit Timestamp (xmt): Time at the server when the response left for the client, in NTP timestamp format.
    ntp_timestamp transmit_timestamp;


    static inline ntp_header from_package(const network_package& package) {
        ntp_header header;

        const uint8_t li = (package.li_vn_mode & 0b11000000) >> 6;
        switch(li) {
            case 0: header.li = leap_indicator::NO_WARNING; break;
            case 1: header.li = leap_indicator::LAST_MINUTE_OF_DAY_61_SECONDS; break;
            case 2: header.li = leap_indicator::LAST_MINUTE_OF_DAY_59_SECONDS; break;
            case 3: header.li = leap_indicator::UNKNOWN_OR_UNSYNCHRONIZED; break;
            default: header.li = leap_indicator::UNKNOWN_OR_UNSYNCHRONIZED; break;
        }

        header.version = (package.li_vn_mode & 0b00111000) >> 3;

        const uint8_t mode = (package.li_vn_mode & 0b00000111) >> 0;
        switch(mode) {
            case 0: header.mode = op_mode::RESERVED; break;
            case 1: header.mode = op_mode::SYMMETRIC_ACTIVE; break;
            case 2: header.mode = op_mode::SYMMETRIC_PASSIVE; break;
            case 3: header.mode = op_mode::CLIENT; break;
            case 4: header.mode = op_mode::SERVER; break;
            case 5: header.mode = op_mode::BROADCAST; break;
            case 6: header.mode = op_mode::NTP_CONTROL_MESSAGE; break;
            case 7: header.mode = op_mode::RESERVED_FOR_PRIVATE_USE; break;
        default: header.mode = op_mode::RESERVED_FOR_PRIVATE_USE; break;
        }

        header.stratum = package.stratum;

        std::memcpy(&header.poll, &package.poll, 1);

        std::memcpy(&header.precision, &package.precision, 1);

        header.root_delay.seconds = ntohs(package.root_delay_seconds);
        header.root_delay.fraction = ntohs(package.root_delay_fraction);

        header.root_dispersion.seconds = ntohs(package.root_dispersion_seconds);
        header.root_dispersion.fraction = ntohs(package.root_dispersion_fraction);

        std::memcpy(header.reference_id, &package.reference_id, 4);

        header.reference_timestamp.seconds = ntohl(package.reference_timestamp_seconds);
        header.reference_timestamp.fraction = ntohl(package.reference_timestamp_fraction);

        header.origin_timestamp.seconds = ntohl(package.origin_timestamp_seconds);
        header.origin_timestamp.fraction = ntohl(package.origin_timestamp_fraction);

        header.receive_timestamp.seconds = ntohl(package.receive_timestamp_seconds);
        header.receive_timestamp.fraction = ntohl(package.receive_timestamp_fraction);

        header.transmit_timestamp.seconds = ntohl(package.transmit_timestamp_seconds);
        header.transmit_timestamp.fraction = ntohl(package.transmit_timestamp_fraction);

        return header;
    }

    std::string pretty_print() {
        const ntp::ntp_header& ntp_package = *this;
        std::stringstream pretty_string;

        pretty_string << "li: ";
        switch(ntp_package.li) {
            case leap_indicator::NO_WARNING: pretty_string << "no warning"; break;
            case leap_indicator::LAST_MINUTE_OF_DAY_61_SECONDS: pretty_string << "last minute of the day has 61 seconds"; break;
            case leap_indicator::LAST_MINUTE_OF_DAY_59_SECONDS: pretty_string << "last minute of the day has 59 seconds"; break;
            case leap_indicator::UNKNOWN_OR_UNSYNCHRONIZED: pretty_string << "unknown/unsynchronized"; break;
        }
        pretty_string << "\n";

        pretty_string << "vn: " << std::to_string(ntp_package.version) << "\n";
        pretty_string << "mode: ";
        switch(ntp_package.mode) {
            case op_mode::RESERVED: pretty_string << "reserved"; break;
            case op_mode::SYMMETRIC_ACTIVE: pretty_string << "symmetric active"; break;
            case op_mode::SYMMETRIC_PASSIVE: pretty_string << "symmetric passive"; break;
            case op_mode::CLIENT: pretty_string << "client"; break;
            case op_mode::SERVER: pretty_string << "server"; break;
            case op_mode::BROADCAST: pretty_string << "broadcast"; break;
            case op_mode::NTP_CONTROL_MESSAGE: pretty_string << "NTP control message"; break;
            case op_mode::RESERVED_FOR_PRIVATE_USE: pretty_string <<  "reserved for private use"; break;
        default: pretty_string << "unknown!"; break;
        }
        pretty_string << "\n";

        std::string stratumstring;
        switch(ntp_package.stratum) {
            case 0      : stratumstring = "unspecified or invalid"; break;
            case 1      : stratumstring = "primary server (e.g., equipped with a GPS receiver)"; break;
            case 2 ... 15   : stratumstring = "secondary server (via NTP)"; break;
            case 16     : stratumstring = "unsynchronized"; break;
            case 17 ... 255 : stratumstring = "reserved"; break;
            default: stratumstring = "unknown!"; break;
        }
        pretty_string << "stratum " << std::to_string(ntp_package.stratum) << " : " << stratumstring << "\n";

        pretty_string << "poll " << std::to_string(ntp_package.poll) << " : " << std::pow(2, ntp_package.poll) << " seconds \n";
        pretty_string << "precision " << std::to_string(ntp_package.precision) << " : " << std::pow(2, ntp_package.precision) << " seconds \n";
        pretty_string << "-------------------------------------- \n";
        pretty_string << "root_delay: " << std::to_string(ntp_package.root_delay.seconds) << "." << std::to_string(ntp_package.root_delay.fraction) << "\n";
        pretty_string << "root_dispersion: " << std::to_string(ntp_package.root_dispersion.seconds) << "." << std::to_string(ntp_package.root_dispersion.fraction) << "\n";
        pretty_string << "-------------------------------------- \n";
        pretty_string << "reference_id: ";
        unsigned char ipv4[4];
        std::memcpy(ipv4, ntp_package.reference_id, 4);
        switch(stratum) {
            case 0: pretty_string << "KOD: " << ntp_package.reference_id << "\n"; break;
        case 1: pretty_string << "Clock ID: " << ntp_package.reference_id << "\n"; break;
        default: pretty_string << "ipv4: " << std::to_string(ipv4[0]) << "." << std::to_string(ipv4[1]) << "." << std::to_string(ipv4[2]) << "." << std::to_string(ipv4[3])
                               << " ipv6 (md5): " << std::hex << ipv4[0] << ipv4[1] << ipv4[2] << ipv4[3] << "\n"; break;
        }
        pretty_string << "-------------------------------------- \n";
        pretty_string << "reference_timestamp: " << std::to_string(ntp_package.reference_timestamp.seconds) << "." << std::to_string(ntp_package.reference_timestamp.fraction) << "\n";
        pretty_string << "origin_timestamp: "  << std::to_string(ntp_package.origin_timestamp.seconds) << "." << std::to_string(ntp_package.origin_timestamp.fraction) << "\n";
        pretty_string << "receive_timestamp: " << std::to_string(ntp_package.receive_timestamp.seconds) << "." << std::to_string(ntp_package.receive_timestamp.fraction) << "\n";
        pretty_string << "transmit_timestamp: " << std::to_string(ntp_package.transmit_timestamp.seconds) << "." << std::to_string(ntp_package.transmit_timestamp.fraction) << "\n";
        return pretty_string.str();
    }
};

}

#endif // NTP_HPP
