#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include <fstream>

#include <boost/bind.hpp>

#include <asiontp/ntp.hpp>

namespace asiontp {

class client
{
public:
    using timestamp_t = std::chrono::system_clock::time_point;

    template<typename T>
    class Event {
    public:
        Event(timestamp_t timestamp, T data)
            : timestamp(timestamp), data(data)
        {}
        timestamp_t timestamp;
        T data;
    };

    struct MessageEventData {
        /// directly acccessible is only transmit_time
        /// as suggested in the SNTP client section of RFC 5905
        /// the whole package however is accessible through
        /// full_data - as it arrived on the network, so
        /// endianness must be taken care of when using it!
        std::string server;
        timestamp_t request_sent;

        ntp::ntp_timestamp transmit_time;
        std::vector<unsigned char> full_data;
    };
    using MessageEvent = Event<MessageEventData>;

    struct TimeoutEventData {
        std::string server;
        timestamp_t request_sent;
    };
    using TimeoutEvent = Event<TimeoutEventData>;

    struct ErrorEventData {
        boost::system::error_code ec;
    };
    using ErrorEvent = Event<ErrorEventData>;

    using MessageCallback =  std::function<void (const MessageEvent&)>;
    MessageCallback callback_messageEvent = [](const MessageEvent&){};

    using TimeoutCallback =  std::function<void (const TimeoutEvent&)>;
    TimeoutCallback callback_timeoutEvent = [](const TimeoutEvent&){};

    using ErrorCallback = std::function<void (const ErrorEvent&)>;
    ErrorCallback callback_errorEvent = [](const ErrorEvent&){};

    std::string ntpServer;

    boost::posix_time::seconds poll_interval;
    boost::posix_time::seconds timeout_duration;
    boost::asio::deadline_timer poll_timer;
    boost::asio::deadline_timer timeout_timer;

    timestamp_t request_sent;

    boost::asio::ip::udp::socket socket;
    boost::asio::ip::udp::endpoint sender_endpoint;
    boost::asio::ip::udp::resolver resolver;


    //TODO: discuss whether less then 1024b should be ample enough
    boost::array<unsigned char, 1024> recv_buffer;

    //TODO: should probably allow for more granular time types
    client(boost::asio::io_service& io_service,
           std::string ntpServer,
           unsigned int interval_seconds = 64,
           unsigned int timeout_seconds = 1);

    void schedule_poll_timer();

    void receive_from_handler(const boost::system::error_code& ec, std::size_t bytes_transferred );
    void receive_timeout_handler(const boost::system::error_code& ec);

    void interval_tick(const boost::system::error_code& ec);

    void run();

    // TODO: implement stop() as well.
};

}
#endif
