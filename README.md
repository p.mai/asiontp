# asiontp

## Description
small ntp client / polling lib based on and to be used with boost::asio.

## Getting started
Take a look at example/logger to get started.

## License
Published here under GPLv3.

## Todo:
* implement stop() method
* find a more portable way to do ntoh
* more consistent naming
