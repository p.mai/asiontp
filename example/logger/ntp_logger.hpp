#ifndef NTP_LOGGER_HPP
#define NTP_LOGGER_HPP


#include <string>
#include <sstream>

#include <vector>
#include <filesystem>
#include <fstream>

#include <asiontp/client.hpp>


namespace wid {
class NtpLogger {

public:
    NtpLogger(std::string server_url) : pool_url(server_url) {

    }

    std::string pool_url;

    bool auto_reconnect = true;

    void run(std::string directory_prefix = "datalog") {
        std::stringstream dirname;
        dirname << directory_prefix << "/" << "ntp" << "/" << pool_url;
        std::filesystem::create_directories(dirname.str());

        while (auto_reconnect) {
            auto now_time = std::chrono::system_clock::now().time_since_epoch().count();

            std::stringstream ofname_ss;
            ofname_ss << dirname.str() << "/";
            ofname_ss << now_time;
            std::string out_file_name(ofname_ss.str());

            std::ofstream myfile;
            myfile.open(out_file_name);


            boost::asio::io_service io_service;
            // 64 is bog standard
            unsigned int interval_seconds = 64;
            asiontp::client ntp_client(io_service, pool_url, interval_seconds);
            ntp_client.callback_messageEvent = [&myfile](const asiontp::client::MessageEvent& event) {
                std::stringstream ss;
                ss << "{\"time\":" << event.timestamp.time_since_epoch().count()
                   <<  "\", \"type\": \"m\", \"server\": \"" << event.data.server <<
                       "\", \"i\": \"" << event.data.transmit_time.seconds << ", \"f\": \"" << event.data.transmit_time.fraction  << "\", \"sent\": \"" << event.data.request_sent.time_since_epoch().count()
                    << "\"}";
                auto event_string = ss.str();
                std::cout << event_string << std::endl;
                myfile << event_string << std::endl;
            };

            ntp_client.callback_timeoutEvent = [&myfile](const asiontp::client::TimeoutEvent& event) {
                std::stringstream ss;
                ss << "{\"time\":" << event.timestamp.time_since_epoch().count() <<  "\", \"type\": \"to\", \"server\": \"" << event.data.server <<  "\", \"sent\": \"" << event.data.request_sent.time_since_epoch().count() << "\"}";
                auto event_string = ss.str();
                std::cerr << event_string << std::endl;
                myfile << event_string << std::endl;
            };

            ntp_client.callback_errorEvent = [&myfile](const asiontp::client::ErrorEvent& event) {
                std::stringstream ss;
                ss << "{\"time\":" << event.timestamp.time_since_epoch().count() <<  "\", \"type\": \"e\", \"value\":" << event.data.ec.value() << "}";
                auto event_string = ss.str();
                std::cerr << event_string << std::endl;
                myfile << event_string << std::endl;
            };


            ntp_client.run();
            io_service.run();


            myfile.close();
        }
    }

};

}
#endif // NTP_LOGGER_HPP
