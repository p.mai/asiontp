#include <iostream>
#include <string>

#include "ntp_logger.hpp"

int main(int, char **) {
  while (true) {
    std::cout << "starting ntpLogger " << std::endl;
    wid::NtpLogger ntpLogger("de.pool.ntp.org");
    ntpLogger.run();
    std::cout << "ntplogger stopped..." << std::endl;
  }
}
