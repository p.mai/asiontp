#include <asiontp/client.hpp>

namespace asiontp {

client::client(boost::asio::io_service &io_service, std::string ntpServer,
               unsigned int interval_seconds, unsigned int timeout_seconds)
    : ntpServer(ntpServer), poll_interval(interval_seconds),
      timeout_duration(timeout_seconds), poll_timer(io_service, poll_interval),
      timeout_timer(io_service, timeout_duration),
      socket(io_service,
             boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), 32124)),
      resolver(io_service) {
  // noop
}

void client::schedule_poll_timer() {
  auto next_event_time = poll_timer.expires_at() + poll_interval;
  poll_timer.expires_at(next_event_time);
  poll_timer.async_wait(
      boost::bind(&client::interval_tick, this, boost::placeholders::_1));
}

void client::receive_from_handler(const boost::system::error_code &ec,
                                  std::size_t bytes_transferred) {
  if (ec == boost::asio::error::operation_aborted) {
    // means this was called with socket's close()/cancel(), not because
    // something was received
    return;
  }

  // turn off timer since we received a datagram (or a different error) before
  // timeout
  timeout_timer.cancel();

  if (ec) {
    ErrorEventData eed;
    eed.ec = ec;
    callback_errorEvent(ErrorEvent(std::chrono::system_clock::now(), eed));
  } else {
    MessageEventData ted;

    auto timestamp = std::chrono::system_clock::now();

    ted.server = ntpServer;
    ted.request_sent = request_sent;

    auto &data_ = recv_buffer;

    // actual packet structure at https://www.rfc-editor.org/rfc/rfc5905#page-17
    // TODO: ntoh?
    typedef boost::uint32_t u32;
    const u32 iPart(
        static_cast<u32>(data_[40]) << 24 | static_cast<u32>(data_[41]) << 16 |
        static_cast<u32>(data_[42]) << 8 | static_cast<u32>(data_[43]));
    const u32 fPart(
        static_cast<u32>(data_[44]) << 24 | static_cast<u32>(data_[45]) << 16 |
        static_cast<u32>(data_[46]) << 8 | static_cast<u32>(data_[47]));

    ted.transmit_time.seconds = iPart;
    ted.transmit_time.fraction = fPart;
    ted.full_data = std::vector<unsigned char>(
        recv_buffer.data(), recv_buffer.data() + bytes_transferred);

    const ntp::network_package package =
        ntp::network_package::from_buffer(data_.data(), bytes_transferred);
    ntp::ntp_header ntp_header = ntp::ntp_header::from_package(package);

    MessageEvent te(timestamp, ted);
    callback_messageEvent(te);
  }

  schedule_poll_timer();
}

void client::receive_timeout_handler(const boost::system::error_code &ec) {
  auto now_time = std::chrono::system_clock::now();
  if (ec == boost::asio::error::operation_aborted) {
    // means this was called with close()/cancel(), not because the timer ran
    // out
    return;
  }

  // close socket since it was timed out.
  // cancel() is not used since it has all kinds of
  // implications regarding os and threading (see boost doc.)
  socket.close();

  TimeoutEventData ted;
  ted.server = ntpServer;
  ted.request_sent = request_sent;

  TimeoutEvent te{now_time, ted};
  callback_timeoutEvent(te);

  schedule_poll_timer();
}

void client::interval_tick(const boost::system::error_code &ec) {
  if (ec) {
    ErrorEventData eed;
    eed.ec = ec;
    callback_errorEvent(ErrorEvent(std::chrono::system_clock::now(), eed));
  }

  // no leap, version 4, mode client
  uint8_t code = 0b00100011;
  boost::uint8_t data[48] = {code, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                             0,    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                             0,    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(),
                                              ntpServer, "ntp");

  boost::system::error_code resolve_error;
  auto const resolve_results = resolver.resolve(query, resolve_error);
  if (resolve_error) {
    ErrorEventData eed;
    eed.ec = resolve_error;
    callback_errorEvent(ErrorEvent(std::chrono::system_clock::now(), eed));
    schedule_poll_timer();
    return;
  }

  // a successful (= non-error) call guarantees results not to be empty
  boost::asio::ip::udp::endpoint receiver_endpoint = *resolve_results;

  if (!socket.is_open()) {
    boost::system::error_code ec;
    socket.open(boost::asio::ip::udp::v4(), ec);

    if (ec) {
      ErrorEventData eed;
      eed.ec = ec;
      callback_errorEvent(ErrorEvent(std::chrono::system_clock::now(), eed));
    }
  }

  auto now_time = std::chrono::system_clock::now();
  request_sent = now_time;
  socket.send_to(boost::asio::buffer(data), receiver_endpoint);

  socket.async_receive_from(
      boost::asio::buffer(boost::asio::buffer(recv_buffer)), sender_endpoint,
      boost::bind(&client::receive_from_handler, this, boost::placeholders::_1,
                  boost::placeholders::_2));

  timeout_timer.expires_from_now(boost::posix_time::seconds(5));
  timeout_timer.async_wait(boost::bind(&client::receive_timeout_handler, this,
                                       boost::placeholders::_1));
}

void client::run() {
  poll_timer.expires_from_now(boost::posix_time::seconds(0));
  poll_timer.async_wait(
      boost::bind(&client::interval_tick, this, boost::placeholders::_1));
}

} // namespace asiontp
